package dxc;

import java.util.Scanner;

public class MetroBank {

	public static void main(String[] args) 
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the account number");
		String ac_no=sc.next();
		if(!ac_no.matches("1[0-9]{3}"))
		{
			System.out.println("Enter valid account number : ");
		}
		System.out.println("Enter the account balance in $ : ");
		long balance=sc.nextLong();
		if(balance<1000)
		{
			System.out.println("Minimum balance should be $1000");
		}
		System.out.println("Enter the salary : ");
		long salary=sc.nextLong();
		System.out.println("Choose one of the Loan type : Car , House , Business :");
		String loan_type=sc.next();
		System.out.println("Enter expected loan amount : ");
		long expected_loan=sc.nextLong();
		System.out.println("Enter expected number of EMIs : ");
		int expected_EMIs=sc.nextInt();
		long eligible_loan = 0;
		int eligible_emis=0;
		if(salary>25000)
		{
			if(loan_type.equalsIgnoreCase("Car"))
			{
				if(expected_loan<=500000)
				{
					eligible_loan=500000;
				}
				if(expected_EMIs<=36)
				{
					eligible_emis=36;
				}
			}

		}
		if(salary>50000)
		{
			if(loan_type.equalsIgnoreCase("House"))
			{
				if(expected_loan<=6000000)
				{
					eligible_loan=6000000;
				}
				if(expected_EMIs<=60)
				{
					eligible_emis=60;
				}
			}

		}
		if(salary>75000)
		{
			if(loan_type.equalsIgnoreCase("Business"))
			{
				if(expected_loan<=7500000)
				{
					eligible_loan=7500000;
				}
				if(expected_EMIs<=84)
				{
					eligible_emis=84;
				}
			}

		}
		
		System.out.println("Eligible Loan amount is : "+eligible_loan);
		System.out.println("Eligible EMIs are : "+eligible_emis);
	}

}
