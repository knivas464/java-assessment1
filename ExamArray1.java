package dxc;

import java.util.Scanner;

public class ExamArray1 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int[] prices={200, 250, 300};
		int[] cart=new int[3];
		System.out.println("Enter the number of Round-neck tshirts:");
		cart[0]=sc.nextInt();
		System.out.println("Enter the number of collared tshirts:");
		cart[1]=sc.nextInt();
		System.out.println("Enter the number of hooded tshirts:");
		cart[2]=sc.nextInt();
		int totalPrice=0;
		for (int i=0;i < 3;i++){
		  totalPrice+=cart[i]*prices[i];
		}
		int total_Shirts=cart[0]+cart[1]+cart[2];
		int discount = 0;
		if(total_Shirts<5)
		{
			discount=0;
		}
		else if(total_Shirts>=5 && total_Shirts<10)
		{
			discount=(int) (0.1*totalPrice);
			System.out.println(discount);
		}
		else if(total_Shirts>=10)
		{
			discount=(int) (0.2*totalPrice);
		}
		
		int final_price=totalPrice-discount;
		
		System.out.println("Final price is RS."+final_price);

	}

}
